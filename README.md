# backend-quiz-service-1 - combine #
Web Service retrieves the data from two JSON feeds and combines the data on the ID property.  

### Dependencies ###
Implemented in Node.js v.0.10.*  
Node built in modules (http and url)  

### Source Descriptions ###
service.js - http server, logic to request feeds and combine JSON  
webRequest.js - http web request class  
config.js - environment settings, feed locations  

### Routes ###
**GET** /combine  
Description: combine json feeds  
Example: http://cn-games-services.turner.com:3101/combine  