var http = require("http");

// HttpCall Class //////////////////////////////
var webRequest = function(options, callback) {
    var request = http.request(options, function(res) {
        var self = this,
            data = "";

        res.setEncoding("utf8");

        res.on("data", function(chunk) {
            data += chunk;
        });

        res.on("error", function(err) {
            emitResp("error", err);
        });

        res.on("end", function() {
            var parsedData = null;
            
            // test for valid JSON
            try {
                parsedData = JSON.parse(data);
            } catch (err) {
                emitResp("error", "JSON parsing failed. - " + err);
                return;
            }

            // test for error in parsedData
            if (parsedData) {
                if (!parsedData.error) {
                    emitResp("success", parsedData);
                } else {
                    emitResp("error", parsedData.error);
                    return;
                }
            } else {
                emitResp("error", "Unknown.");
                return;
            }
        });

        function emitResp(call, resp) {
            self.emit(call, resp);
        }
    });

    // end request
    request.end();

    // error handler
    request.on("error", function(err) {
        callback(new Error(err));
    });

    // success handler
    request.on("success", function(parsedData) {
        callback(null, parsedData);
    });
};

module.exports = webRequest;