// Environment Vars
var appPort = process.env.APPPORT || 3101;

// Config Vars
var config = {
    server: {
		listenIP: "0.0.0.0",
		port: appPort
	},

	dataFeed: {
		shows: "http://www.cartoonnetwork.com/test/backend-quiz/shows.json",
		games: "http://www.cartoonnetwork.com/test/backend-quiz/games.json"
	}
};

module.exports = config;
