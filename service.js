var http = require("http"),
    url = require("url"),
    config = require("./config"),
    webRequest = require("./webRequest");

function reqData(urls, callback) {
    // loop urls array
    // execute web request for each URL passed in
    // push resulting JSON into resArr
    // return with callback

    var resArr = [];

    urls.forEach(function (uri) {
        // parse url
        var reqOptions = {
            host: url.parse(uri).hostname,
            path: url.parse(uri).path
        };

        // request data
        webRequest(reqOptions, function(err, res) {
            if (err) {
                // return error
                callback(err);
            } else {
                // get first first Object from JSON (games or shows) 
                var firstObj = Object.keys(res)[0];
                
                // push to result array
                resArr.push(res[firstObj]);

                // if all urls have been processed, return array with data
                if (resArr.length == urls.length) {
                    callback(null, resArr);
                }
            }
        });
    });
}

function combineArrays(array1, array2) {
    // loop first array
    // loop second array
    // match on id
    // loop properties of obj matching on id
    // if property doesn't exist in first array, add it

    for (var o in array1) {
        for (var p in array2) {
            if (array1[o].id == array2[p].id) {
                for (var prop in array2[p]) {
                    if (!(prop in array1[o])) {
                        array1[o][prop] = array2[p][prop];
                    }
                }
            }
        }
    }

    return array1;
}

http.createServer(function (request, response) {
    // web server

    var uri = url.parse(request.url).pathname;

    if (uri.indexOf("combine") != -1) {
        console.log("[info] combine endpoint called");
        
        // request JSON feeds
        reqData([config.dataFeed.games, config.dataFeed.shows], function(err, res) {
            if (err) {
                // error retriving JSON feeds
                console.error([err] + "500 - " + err);

                response.writeHead(500, {
                    "Content-Type": "text/plain"
                });             
                
                // error message
                response.end("500 - " + err);
            } else {
                response.writeHead(200, {
                    "Content-Type": "application/json"
                });

                // combine JSON ane return in response                
                response.end(JSON.stringify(combineArrays(res[0], res[1])));
            }
        });
    } else {
        console.log("[info] 404");
        response.writeHead(404, {
            "Content-Type": "text/plain"
        });
        response.end("404 - Not Found");
    }
}).listen(config.server.port, config.server.listenIP, function() {
    console.log("Web server listening on " + config.server.listenIP + ":" + config.server.port);
});